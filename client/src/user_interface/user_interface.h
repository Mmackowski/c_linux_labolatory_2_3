typedef struct UserInterface
{
	server_conn * server;
	int (* print_user_interface)(void);
	int (* main_loop) (struct UserInterface * self);

} UserInterface;

UserInterface * CreateUserInterface();
void DeleteUserInterface(UserInterface * UI);
