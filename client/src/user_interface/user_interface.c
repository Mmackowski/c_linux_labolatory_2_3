#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../server_connection/server_connection.h"
#include "user_interface.h"
#include "../../../utils/message_structures.h"

enum choice {list = 1,data,phys_addr,ip_and_mask,exit_prog};

int print_user_interface()
{
	printf("Choose one of the following options:\n\
1. get interface list.\n\
2. display interface data.\n\
3. set interface physical address.\n\
4. set interface ip and netmask. \n\
5. exit.\n\
\
your choice : ");
	return 0;
}
void printInterfaceList(msg_ifnames_asw * data)
{
	printf("\n\n ====Interface-list-on-remote-server====\n");
	int i = 0;
	char * pos_in_buff = (char*)data + sizeof(msg_ifnames_asw);
	for(;i<data->if_count;++i)
	{
		printf(" -- %s \n",pos_in_buff);
		pos_in_buff = pos_in_buff + strlen(pos_in_buff) + 1;
	}
	printf(" ======================================\n\n");
}	

int main_loop(UserInterface * self)
{
	bool cont = true;
	int choice=0;
	do {
		print_user_interface();
		int k = 0 ;
		if(!(k = scanf("%d",&choice)))
		{	
			printf("wrong input scanf");
			char c;
			while ((c = getchar()) != '\n' && c != EOF);
			continue;
		}

		switch(choice)
		{
			case list:
				printf("choosen interface list\n");
				msg_type * message;
				message = malloc(sizeof(msg_type));
				memset(message,0,sizeof(msg_type));
				message->type = MSG_IF_NAME_REQ;
				self->server->send_request(self->server,message,sizeof(msg_type));
				free(message);
				msg_ifnames_asw * answer = self->server->receive(self->server);
				printInterfaceList(answer);
				break;
			case data:
				printf("chosen interface data\n");
				break;
			case phys_addr:
				printf("chosen setting interface physical address\n");
				break;
			case ip_and_mask:
				printf("chosen setting ip and netmask\n");
				break;
			case exit_prog:
				printf(" exitting ...\n");
				cont = false;
				break;
			default:
				printf("no such option\n");
				break;
		}
	} while(cont);
	return 0;
}

UserInterface * CreateUserInterface(server_conn * server)
{
	UserInterface * UI = malloc(sizeof(UserInterface));
	
	if(!UI)
	{
		printf("cannot allocate memory\n");
		return  (UserInterface *) -1;
	}

	memset(UI,0,sizeof(UserInterface));
	UI->server = server;	
	UI->print_user_interface = print_user_interface;
	UI->main_loop = main_loop;

	return UI;
}

void DeleteUserInterface(UserInterface * UI)
{
	free(UI);
}
