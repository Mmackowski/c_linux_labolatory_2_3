#include <stdio.h>
#include "server_connection/server_connection.h"
#include "user_interface/user_interface.h"


int main()
{
	server_conn * server = CreateServerConnection();
	if(server->init_conn(server))
		return 1;
	UserInterface * UI = (UserInterface *) CreateUserInterface(server);
	UI->main_loop(UI);
        printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
	DeleteUserInterface(UI);
	DeleteServerConnection(server);	
	return 0;
}
