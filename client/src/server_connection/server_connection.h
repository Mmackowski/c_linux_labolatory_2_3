

typedef struct server_conn {
	int socketFD;
	int (*init_conn)(struct server_conn * self);
	int (*send_request)(struct server_conn * self,void * buff,size_t size);
	void * (*receive)(struct server_conn * self);
} server_conn;

server_conn * CreateServerConnection();
void DeleteServerConnection(server_conn *);
