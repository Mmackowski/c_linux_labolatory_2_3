#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include "server_connection.h"
#include "../../../utils/message_structures.h"

int init_conn(server_conn * self)
{

	int socket_ret_value;
	socket_ret_value = socket(AF_INET, SOCK_STREAM,IPPROTO_TCP);
	if(socket<0)
	{
		printf("could not open socket\n");
		return 1;
	}
	self->socketFD = socket_ret_value;
	
	struct sockaddr_in addr;
        addr.sin_family = AF_INET;
        if(inet_aton("127.0.0.1",&(addr.sin_addr))<=0)
	{
		printf("inet_aton failed\n");
		return 2;
	}
        addr.sin_port = htons(46);
	if(connect(self->socketFD,(struct sockaddr *)&addr,sizeof(addr))<0)
	{
		printf("connection failed\n");
		return 3;
	}
	printf("succesfully connected\n");
	return 0;	
}

int send_request(server_conn * self,void * buff,size_t size)
{
	send(self->socketFD,buff,size,0);
	return 0;
}

void * receive(server_conn * self)
{	
	msg_ifnames_asw msg;
	recv(self->socketFD,&msg,sizeof(msg_ifnames_asw),MSG_PEEK);
	void * msg_buff = malloc(sizeof(msg_ifnames_asw)+msg.name_buff_len);	
	memset(msg_buff,0,sizeof(msg_ifnames_asw)+msg.name_buff_len);
	int bytes = recv(self->socketFD,msg_buff,sizeof(msg_ifnames_asw)+msg.name_buff_len,0);
	printf("\n\nin secound recv read %d bytes\n\n",bytes);
	return msg_buff;
}

server_conn * CreateServerConnection()
{
	server_conn * server = malloc(sizeof(server_conn));
	if(!server)
	{
		printf("could not allocate memory");
		return (server_conn * ) 0;
	}
	memset(server,0,sizeof(server_conn));
	server->init_conn = init_conn;
	server->send_request = send_request;
	server->receive = receive;
	server->socketFD = 0;
	return server;
}

void DeleteServerConnection(server_conn * server)
{
	free(server);
}
