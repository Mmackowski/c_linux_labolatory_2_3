
#define MSG_IF_NAME_REQ 1
#define MSG_IF_NAME_ASW 2
#define MSG_IF_DATA_REQ 3
#define MSG_IF_DATA_ASW 4

typedef struct msg_type {
	char type;
} msg_type;

typedef struct msg_ifnames_asw {
	char type;
	char if_count;
	char name_buff_len; 
} msg_ifnames_asw;

typedef struct ms_ifdata_req {
	char type;
	int if_list_len;
} ms_ifdata_req;
