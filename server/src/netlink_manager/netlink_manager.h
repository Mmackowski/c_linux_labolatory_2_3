
typedef struct netlink_manager {
	int NetlinkFD;
	int (*init)(struct netlink_manager *);
	int (*request_address)(struct netlink_manager *,char *);	
} netlink_manager;

netlink_manager * CreateNetlinkManager();
void DeleteNetlinkManager(netlink_manager *)
