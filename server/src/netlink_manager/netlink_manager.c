#include <bits/sockaddr.h>
#include <asm/types.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "netlink_manager.h"
#include <net/if.h>

int parse_nl_response(struct nlmsghdr *  header)
{
	struct ifinfomsg *iface;
	struct rtattr *attribute;
	int len;

  	iface = NLMSG_DATA(header);
  	len = header->nlmsg_len - NLMSG_LENGTH(sizeof(*iface));
	for (attribute = IFLA_RTA(iface); RTA_OK(attribute, len); attribute = RTA_NEXT(attribute, len))
    	{
		struct in_addr * addr;
      		switch(attribute->rta_type)
      		{
        		case IFA_ADDRESS: 	
				addr =  (struct in_addr *) RTA_DATA(attribute);
				char * s = inet_ntoa(*addr);
          			printf("interface lo address is: %s",s);
				break;
			case IFA_UNSPEC:
				printf("IFA_UNSPEC\n");
				break;
			case IFA_LOCAL:
				addr =  (struct in_addr *) RTA_DATA(attribute);
				s = inet_ntoa(*addr);
          			printf("interface lo address is: %s",s);
				printf("IFA_LOCAL\n");
				break;
			case IFA_LABEL:
				printf("IFA_LABEL\n");
				break;
			case IFA_BROADCAST:
				printf("IFA_BROADCAST\n");
				break;
			case IFA_ANYCAST:    
				printf("IFA_ANYCAST\n");
				break;
			case IFA_CACHEINFO:
				printf("IFA_CACHEINFO\n");
				break;
			default:
				printf("\n%d\n",attribute->rta_type);
				break;
		}
	}
	return 0;
}

int request_address(netlink_manager * self,char * ifname)
{
	struct {
               struct nlmsghdr  nh;
               struct ifaddrmsg ifaddr;
               char attrbuf[128];
           } req,resp;
	memset(&req, 0, sizeof(req));
	req.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
        req.nh.nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
        req.nh.nlmsg_type = RTM_GETADDR;
	req.nh.nlmsg_pid  = getpid();
 	req.nh.nlmsg_seq = 1;
	req.ifaddr.ifa_family = AF_INET;
        req.ifaddr.ifa_index = if_nametoindex(ifname);
	struct sockaddr_nl la;
	la.nl_family = AF_NETLINK;
        la.nl_pad = 0;
        la.nl_pid = 0;
        la.nl_groups = 0;
	struct iovec iov = { &req, req.nh.nlmsg_len };
	struct msghdr msg = {
	&la, sizeof(struct sockaddr_nl),&iov,1,NULL,0,0 
	};
	sendmsg(self->NetlinkFD, &msg, 0);
	memset(&resp,0,sizeof(resp));
	recv(self->NetlinkFD,&resp,sizeof(resp),0);
	parse_nl_response(&(resp.nh));
	return 0;
}

int init(netlink_manager * self)
{
	self->NetlinkFD = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);	
	if(!self->NetlinkFD)
	{
		printf("could not open netlink socket\n");
		return 1;	
	}
	struct sockaddr_nl addr;
	addr.nl_family = AF_NETLINK;
	addr.nl_pad = 0;
	addr.nl_pid = getpid();
	addr.nl_groups = 0;	
	int binded = bind(self->NetlinkFD,(struct sockaddr *)&addr,sizeof(struct sockaddr_nl));
	if(binded!=0)
		printf("netlink socket binding failed\n");	
	return 1;
}

netlink_manager * CreateNetlinkManager()
{
	netlink_manager * nl_manager = malloc(sizeof(netlink_manager));
	if(!nl_manager)
	{
		printf("could not allocate memory for netlink_manager\n");
		return nl_manager;
	}
	nl_manager->NetlinkFD = 0;
	nl_manager->init = init;
	nl_manager->request_address = request_address;
	return nl_manager;
}

void DeleteNetlinkManager(netlink_manager * nl_manager)
{
	close(nl_manager->NetlinkFD);
	free(nl_manager);
}
