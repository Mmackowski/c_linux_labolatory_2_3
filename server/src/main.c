#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include "../../utils/message_structures.h"
#include "ifaddrs.h"
#include <stdlib.h>

#define BUFF_SIZE 20
#define DRY_RUN 1
#define COLLECT_NAMES 0

void handle_message( char *,int clientFD);
msg_ifnames_asw * get_interface_names_answer(void);
int iterate_through_interface_list(struct ifaddrs *,char * , int *, int);
void print_message_struct(msg_ifnames_asw *);

int main()
{
	printf("I am server\n");
	int socketFD = socket(AF_INET, SOCK_STREAM,IPPROTO_TCP);
	
	if ( socketFD < 0 )
	{
		printf("could not create socket\n");
		return 1;
	}
	
	int enable = 1;
	
	if( setsockopt(socketFD,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int)) < 0 )
	{
		printf("could not set SO_REUSEADDR by setsockopt\n");
		return 2;
	}
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	inet_aton("127.0.0.1",&(addr.sin_addr));
	addr.sin_port = htons(46);
	
	bind(socketFD,(struct sockaddr *)&addr,sizeof(struct sockaddr_in));
	listen(socketFD,1);

	int client = accept(socketFD,NULL,NULL);
	printf("accepted incoming client connection\n");
	char buff[BUFF_SIZE];
	memset(buff,0,BUFF_SIZE);
	recv(client,buff,(size_t)BUFF_SIZE,0);
	handle_message(buff,client);
	return 0;
}

void handle_message(char * buff,int clientFD)
{
	msg_type * message = (msg_type *) buff;
	switch(message->type)
	{
		case MSG_IF_NAME_REQ:
		printf("received interface names list request\n");
		msg_ifnames_asw * reply = get_interface_names_answer();
		if(reply)
			send(clientFD,(void *)reply,sizeof(msg_ifnames_asw)+reply->name_buff_len,0);
		break;
	}
}

void print_message_struct(msg_ifnames_asw * msg)
{
	printf("\n messge type: %d\nnumber of interfaces %d\nname buffer length %d\ninterface names\n",msg->type,msg->if_count,msg->name_buff_len);
	int i = msg->if_count;
	int offset = sizeof(msg_ifnames_asw);
	int k;
	for(k=0;k<i;++k)
	{
		printf("== %s\n",((char *)msg+offset));
		offset += strlen((char *)msg+offset) + 1; 
	}
	
}

msg_ifnames_asw * get_interface_names_answer()
{
	struct ifaddrs * interfaces;
	int name_buff_len = 0;
	getifaddrs(&interfaces);
	int if_count  = iterate_through_interface_list(interfaces,NULL,&name_buff_len,DRY_RUN);
	msg_ifnames_asw * reply = malloc(sizeof(msg_ifnames_asw) + name_buff_len);
	if(!reply)
	{
		printf("could not allocate memory\n");
		return reply;
	}
	memset(reply,0,sizeof(msg_ifnames_asw));
	reply->type = MSG_IF_NAME_ASW;
	reply->if_count = if_count;
	reply->name_buff_len = name_buff_len;
	iterate_through_interface_list(interfaces,(char *)reply+sizeof(msg_ifnames_asw),NULL,COLLECT_NAMES);
	freeifaddrs(interfaces);
	print_message_struct(reply);
	return reply;	
}

int iterate_through_interface_list(struct ifaddrs * first_if,char * if_name_buff, int * bytes_for_name_list, int flags)
{
	struct ifaddrs * tmp_if = first_if;
	int offset = 0,if_count = 0;
	while(tmp_if!=NULL)
	{
		if(tmp_if->ifa_addr && tmp_if->ifa_addr->sa_family != AF_PACKET)
		{
			tmp_if = tmp_if->ifa_next;
			continue;
		}
		size_t if_name_len = strlen(tmp_if->ifa_name);
		if( if_name_buff && flags == COLLECT_NAMES )
		{
			memcpy(if_name_buff + offset,tmp_if->ifa_name,if_name_len +1);
		}
		offset += ( if_name_len + 1 );
		//printf("%s\n",tmp_if->ifa_name);
		++if_count;
		tmp_if = tmp_if->ifa_next;
	}
	if(bytes_for_name_list && flags == DRY_RUN)
		*bytes_for_name_list = offset;
	return if_count;
}
